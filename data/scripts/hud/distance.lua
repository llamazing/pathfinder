--[[ distance.lua
	version 0.1a1
	11 Jan 2019
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This HUD menu script displays the distance remaining (in 16x16 tiles) until the target
	destination is reached.
	
	The text to be displayed is defined by the strings.dat key "hud.distance". "%d" should
	be used in place of the numerical value for the displayed distance.
]]

local distance_manager = {}

--// Creates a new menu instance
	--properties (table, key/value) - specifies configuration settings for the initialization of the menu (see below)
	--returns a newly created distance menu
function distance_manager:create(properties)
	local menu = {
		x = 0, y = 0, --(number, integer) coordinates in pixels used to apply a movement to the menu
		pos_x = math.floor(tonumber(properties.x) or -4), --x coordinate of where to draw the menu in pixels, negative values are from the right or the screen
		pos_y = math.floor(tonumber(properties.y) or -4), --y coordinate of where to draw the menu in pixels, negative values are from the bottom of the screen
		
		font = properties.font or "8_bit", --name of the font resource to use for displaying the text
		horizontal_alignment = properties.horizontal_alignment or "right", --string with possible values "left", "right" or "center"
		vertical_alignment = properties.vertical_alignment or "bottom", --string with possible values "top", "middle", "bottom"
		
		text_surface = nil, --sol.text_surface containing the text to be displayed
	}
	
	local distance --(number, integer, positive) the distance in tiles sized 16x16 pixels
	local text --(string) the text to display, format of the number value to display the distance is %d
	
	
	--// Validation
	
	assert(type(menu.font)=="string", "Bad property font to 'distance_builder' (string expected)")
	assert(sol.main.resource_exists("font", menu.font), "Bad property font to 'distance_builder', invalid font name: "..menu.font)
	
	assert(type(menu.horizontal_alignment)=="string", "Bad property horizontal_alignment to 'distance_builder' (string expected)")
	assert(type(menu.vertical_alignment)=="string", "Bad property vertical_alignment to 'distance_builder' (string expected)")
	
	
	--// Implementation
	
	menu.text_surface = sol.text_surface.create{
		font = menu.font,
		horizontal_alignment = menu.horizontal_alignment,
		vertical_alignment = menu.vertical_alignment,
	}
	
	--// Get/set the displayed amount for the distance
	function menu:get_distance() return distance end
	function menu:set_distance(amount)
		distance = math.max(math.floor(tonumber(amount) or 0), 0)
		
		text = sol.language.get_string"hud.distance"
		self.text_surface:set_text(string.format(text, distance))
	end
	menu:set_distance(0)
	
	--// Get/set the position of the menu
	function menu:get_xy() return self.x, self.y end
	function menu:set_xy(x, y)
		self.x = math.floor(tonumber(x) or 0)
		self.y = math.floor(tonumber(x) or 0)
	end
	
	function menu:on_started()
		text = sol.language.get_string"hud.distance"
	end
	
	function menu:on_draw(dst_surface)
		local pos_x, pos_y = self.pos_x, self.pos_y
		local dst_width, dst_height = dst_surface:get_size()
		
		if pos_x < 0 then pos_x = pos_x + dst_width end
		if pos_y < 0 then pos_y = pos_y + dst_height end
		
		self.text_surface:draw(dst_surface, self.x + pos_x , self.y + pos_y)
	end

	return menu
end

setmetatable(distance_manager, {__call = distance_manager.create})

return distance_manager

--[[ Copyright 2019 Llamazing
  [[ 
  [[ This program is free software: you can redistribute it and/or modify it under the
  [[ terms of the GNU General Public License as published by the Free Software Foundation,
  [[ either version 3 of the License, or (at your option) any later version.
  [[ 
  [[ It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [[ without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [[ PURPOSE.  See the GNU General Public License for more details.
  [[ 
  [[ You should have received a copy of the GNU General Public License along with this
  [[ program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
