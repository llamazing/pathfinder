require"scripts/multi_events"
local initial_game = require"scripts/initial_game"

--menus
local distance_menu = require"scripts/hud/distance"{}
local pause_menu = require"scripts/menus/pause"{}
local nav_map = require"scripts/menus/nav_map"

local game_manager = {}

--Creates a game ready to be played.
function game_manager:create(file)

  --Create the game (but do not start it).
	local exists = sol.game.exists(file)
	local game = sol.game.load(file)
	if not exists then
		--This is a new savegame file.
		initial_game:initialize_new_savegame(game)
	end
	
	game.nav_map = nav_map:create(game)
	game.distance_menu = distance_menu
	sol.main.add_resource("map", "generated_map", "generated map")
	game:set_starting_location("generated_map", "start")  -- Starting location.
	
	game:register_event("on_started", function(self)
		sol.menu.start(game, distance_menu)
		sol.menu.start(game, self.nav_map)
	end)
	
	game:register_event("on_paused", function(self)
		sol.menu.start(game, pause_menu)
	end)
	
	game:register_event("on_unpaused", function(self)
		sol.menu.stop(pause_menu)
	end)
	
	function game:set_custom_command_effect() end --do nothing
	
	return game
end

return game_manager
