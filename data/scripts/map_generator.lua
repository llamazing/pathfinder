--[[ map_generator.lua
	version 0.1a1
	10 Feb 2019
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script generates a randomized map with navigation beacons connected by paths.
]]

local util = require"scripts/util"

local map_generator = {}

--// configurable constants

--size of the generated map in pixels
local MAP_SIZE_X = 8192
local MAP_SIZE_Y = 4608

--newly generated nodes must be no more than max distance (in pixels) from an existing
--node and also no closer than min distance to any existing node
local MIN_DISTANCE = 144 --in pixels, must be multiple of 16
local MAX_DISTANCE = 208 --in pixels, must be multiple of 16

local MAP_ID = "generated_map" --resource id of the generated map (added to write directory)


--## calculated constants ##--

--also quest size in pixels
local NUM_COLUMNS = MAP_SIZE_X/16 --512
local NUM_ROWS = MAP_SIZE_Y/16 --288

local MIN_RADIUS = MIN_DISTANCE/16 --9
local MAX_RADIUS = MAX_DISTANCE/16 --13

local MIN_RADIUS_SQ = MIN_RADIUS*MIN_RADIUS --convenience, used multiple times
local MAX_RADIUS_SQ = MAX_RADIUS*MAX_RADIUS --convenience, used multiple times

--properties of the generated map, written to .dat file
local MAP_PROPERTIES = {
	x = 0,
	y = 0,
	width = MAP_SIZE_X,
	height = MAP_SIZE_Y,
	min_layer = 0,
	max_layer = 2,
	tileset = "zoria",
}

--specify tile ids to use when making the paths connecting nodes
local TILES_LOOKUP = {
	path = "grass.soil", --tile id for connecting paths
	node = "grass.weed_big", --tile id for nodes
	--border tiles, index is sum of applicable: 1 (above), 2 (to right), 4 (below) and 8 (to left)
	--value is the tile id
	63, --above
	67, --right
	64, --right and above
	70, --below
	74, --above and below
	71, --right and below
	75, --all but left
	65, --left
	62, --left and above
	68, --left and right
	363, --all but below
	69, --left and below
	73, --all but right
	72, --all but above
	76, --all sides
}

--// Creates a mapper instance used to write a procedurally generated map .dat file
function map_generator:create()
	local mapper = {} --object to be returned containing various functions
	
	local node_list -- (table, array) list of nodes on the current map. Values are a key/value table with the following:
		--x & y (number, positive integer) - location of the node on the map as 16x16 sized tiles
		--connections (table, key/value) - keys are the connected node's index from node_list, value is the distance away in pixels
		--node_type (string, optional) - value for the custom property "node_type"
			--"teletransporter" denotes the node as a teletransporter
	local connections --(table, array) list of all node connections where each entry is a table array with 2 values:
		--(number, positive) for value #1 & #2, the node indicies associated with this connections
	local tiles_data = {} --(table, array) - contains properties of each map tile as key/value table, see add_tile()
	
	--// Makes a new tile with the given properties (table, key/value):
		--x (number, non-negative integer) - x coordinate for the tile's map location
		--y (number, non-negative integer) - y coordinate for the tile's map location
		--layer (number, integer) - map layer of the tile
		--width (number, positive integer) - width of the tile in pixels
		--height (number, positive) - height of the tile in pixels
		--pattern (string) - tile id of the tileset pattern to use for the tile
	local function add_tile(properties)
		if not properties.layer then properties.layer = 0 end
		table.insert(tiles_data, properties)
	end
	
	--// Returns an integer from 0 to 15 indicating which adjacent pixels are on a connecting path
		--a number is assigned for each of the following conditions:
			--0: none of the adjacent tiles are on a path
			--1: the tile above is on a path
			--2: the tile to the right is on a path
			--4: the tile below is on a path
			--8: the tile to the left is on a path
		--returns the sum of all the above values for conditions which are true
			--i.e. if there is a path above and to the right then the value returned is 1+2 = 3
	local function get_adjacent_pixels(data, x, y)
		local above = (data[y-1] and data[y-1][x])==1 and 1 or 0
		local right = (data[y] and data[y][x+1])==1 and 2 or 0
		local below = (data[y+1] and data[y+1][x])==1 and 4 or 0
		local left = (data[y] and data[y][x-1])==1 and 8 or 0
		
		return above + right + below + left
	end
	
	--// Writes the generated map to the quest write directory
	local function write_map()
		sol.file.mkdir"maps" --create a maps directory in the write directory in case it doesn't already exist
		local file = sol.file.open("maps/"..MAP_ID..".dat", "w")
		file:write(string.format([[
properties{
	x = %d,
	y = %d,
	width = %d,
	height = %d,
	min_layer = %d,
	max_layer = %d,
	tileset = "%s",
}

]],
			MAP_PROPERTIES.x,
			MAP_PROPERTIES.y,
			MAP_PROPERTIES.width,
			MAP_PROPERTIES.height,
			MAP_PROPERTIES.min_layer,
			MAP_PROPERTIES.max_layer,
			MAP_PROPERTIES.tileset
		))
		
		for _,tile in ipairs(tiles_data) do
			file:write(string.format([[
tile{
	layer = %d,
	x = %d,
	y = %d,
	width = %d,
	height = %d,
	pattern = "%s",
}

]],
				tile.layer,
				tile.x,
				tile.y,
				tile.width,
				tile.height,
				tile.pattern
			))
		end
		
		--randomly select 3 nodes to be teletransporters
		--[[local tele = {} --TODO delete
		local tele_count = 0
		while tele_count < 3 do
			local index = math.random(#node_list)
			if not tele[index] then
				tele[index] = true
				tele_count = tele_count + 1
			end
		end]]
		local tele_str = [[
sprite = "teletransporters/teletransporter_yellow",
properties = {
	{ key = "node_type", value = "teletransporter" },
},]]		
		for i,node_data in ipairs(node_list) do
			file:write(string.format([[
custom_entity{
	name = "node_%d",
	layer = %d,
	x = %d,
	y = %d,
	width = %d,
	height = %d,
	direction = %d,%s
}

]],
				i,
				0,
				node_data.x*16 - 16 + 8,
				node_data.y*16 - 16 + 13,
				16,
				16,
				0,
				node_data.node_type=="teletransporter" and tele_str or ""
			))
		end
		
		local random_index = math.random(NUM_ROWS*NUM_COLUMNS)
		local random_x = (random_index-1)%NUM_COLUMNS + 1
		local random_y = math.floor((random_index-1)/NUM_COLUMNS) + 1
		
		file:write(string.format([[
destination{
  name = "start",
  layer = 0,
  x = %d,
  y = %d,
  direction = 3,
}

]],
		random_x*16 - 16 + 8,
		random_y*16 - 16 + 13
		))
		
		--NPC to show in place of invisible player (for walking animation)
		file:write([[
npc{
  name = "robyne",
  layer = 0,
  x = 0,
  y = 0,
  direction = 3,
  subtype = 1,
  sprite = "hero/tunic1",
}

]])
		
		print("random start:", random_index, random_x*16 - 16 + 8, random_y*16 - 16 + 13)
		file:close()
	end
	
	--// Randomly chooses locations to place nodes on map until map is full
	function mapper:generate_nodes()
		node_list = {} --create a new node list
		connections = nil --any previous connections are now useless
		
		local coords = {}
		local coord_count = 0
		
		
		--## local functions ##--
		
		--// Marks an individual coordinate pair on the generated map for suitability of
		--// adding a new node there. All coords initially unmarked. Nodes can only be
		--// placed on coords marked good. Once marked bad, a coord pair is no longer
		--// eligible to be marked good.
			--x (number, positive) - x coordinate of the location to be marked
			--y (number, positive) - y coordinate of the location to be marked
			--is_good (boolean) - true marks the coord pair as good, false as bad
		local function mark_coords(x, y, is_good)
			local row = coords[y]
			local old_value = row[x]
			
			if is_good==false then --mark coordinate as bad
				row[x] = -1 --mark as bad
				
				--decrement counts if good coordinate was changed to bad
				if old_value == 1 then
					row.count = row.count - 1
					coord_count = coord_count - 1
				end
			elseif is_good==true then --mark coordinate as good
				if old_value == 0 then --don't change bad coordinate to good
					row[x] = 1 --mark as good
					
					--one more good coordinate to choose from
					row.count = row.count + 1
					coord_count = coord_count + 1
				end
			end
		end
		
		--// Checks map coordinates within a radius of the specified coordinate pair for
		--// suitability of adding a new node. Coords between min & max radius are
		--// eligible to have a node added, coords less than min radius are ineligible.
		--// Coords previously marked ineligible will not be changed.
			--coord_x (number, positive) - x coordinate denoting the center of the radius to check
			--coord_y (number, positive) - y coordinate denoting the center of the radius to check
		local function check_radius_from_coords(coord_x,coord_y)
			local min_y = math.max(coord_y - MAX_RADIUS, 1) --top-most row that is within MAX_RADIUS
			local max_y = math.min(coord_y + MAX_RADIUS, NUM_ROWS) --bottom-most row that is within MAX_RADIUS
			for y = min_y, max_y do --go top to bottom for all rows within MAX_RADIUS range
				local delta_y = math.abs(coord_y - y)
				local delta_y_sq = delta_y*delta_y --convenience, used multiple times
				
				local max_delta_x = math.floor(math.sqrt(MAX_RADIUS_SQ - delta_y_sq))
				local min_x = math.max(coord_x - max_delta_x, 1) --farthest coord to the left on this row that is within MAX_RADIUS
				local max_x = math.min(coord_x + max_delta_x, NUM_COLUMNS) --farthest coord to the right on this row that is within MAX_RADIUS
				
				local min_diff = MIN_RADIUS_SQ - delta_y_sq
				if min_diff <=0 then --all coords between min_x & max_x are valid
					for x = min_x, max_x do
						mark_coords(x, y, true) --this location is eligible to add a node
					end
				else
					local mid_delta_x = math.sqrt(min_diff) --dx must be greater than or equal in order to be valid
					local mid_x_lower = math.floor(coord_x - mid_delta_x) --valid if x is less than or equal to (may be non-integer)
					local mid_x_upper = math.ceil(coord_x + mid_delta_x) --valid if x is greater than or equal to (may be non-integer)
					
					--lower valid range
					for x = min_x, mid_x_lower do
						mark_coords(x, y, true) --this location is eligible to add a node
					end
					
					--middle invalid range
					local mid_start = math.max(mid_x_lower + 1, 1)
					local mid_stop = math.min(mid_x_upper - 1, NUM_COLUMNS)
					for x = mid_start, mid_stop do
						mark_coords(x, y, false) --this location is too close to an existing node to add a new node here
					end
					
					--upper valid range
					for x = mid_x_upper, max_x do
						mark_coords(x, y, true) --this location is eligible to add a node
					end
				end
			end
		end
		
		--// Creates a new node at the given coordinates then marks surrounding location
		--// as either eligible or ineligible to add new nodes
			--x (number, positive integer) - x coordinate of the node location as 16x16 tiles
			--y (number, positive integer) - y coordinate of the node location as 16x16 tiles
		local function create_node(x, y)
			table.insert(node_list, {x=x, y=y, connections={}})
			check_radius_from_coords(x, y)
			return true
			
			--TODO return false if already a node at this location
		end
		
		--// Randomly selects a location to add a new node from among eligible locations.
		--// Guaranteed to add new node unless the map is full (no eligible locations)
			--returns true if successful, otherwise false
		local function find_new_node()
			if coord_count > 0 then
				local new_coord = math.random(coord_count) --randomly choose any available coordinate
				
				--find the coordinate corresponding to random selection
				local total = 0
				for y = 1, NUM_ROWS do
					local row = coords[y]
					local new_total = total + row.count
					
					if new_coord <= new_total then --coordinate is in this row
						for x,status in ipairs(row) do
							if status==1 then
								total = total + 1
								if new_coord == total then --found coordinate
									create_node(x,y)
									return true
								end
							end
						end
					else total = new_total end
				end
			else return false end --no coordinates available for new node
		end
		
		
		--## implementation ##--
		
		--create blank coordinate table of map
		for y = 1, NUM_ROWS do
			local row = {}
			for x = 1, NUM_COLUMNS do row[x] = 0 end
			
			row.count = 0
			coords[y] = row
		end
		
		--generate first node (add it anywhere)
		do
			local new_coord = math.random(NUM_ROWS*NUM_COLUMNS) --randomly choose any available coordinate
			local y = math.floor((new_coord - 1) / NUM_COLUMNS) + 1
			local x = ((new_coord - 1) % NUM_COLUMNS) + 1
			create_node(x, y)
		end
		
		while find_new_node() do end --continue to generate new nodes until map is full
		
		return node_list
	end
	
	--// Randomly selects the specified number of nodes to act as teletransporters
		--count (number, non-negative integer) the number of teletransporters to designate
	function mapper:designate_teletransporters(count)
		count = tonumber(count)
		assert(count, "Bad argument #2 to 'designate_teletransporters' (number expected)")
		assert(count >= 0, "Bad argument #2 to 'designate_teletransporters', number must be non-negative")
		
		--TODO ensure each teletransporter is a minimum distance away from other teletransporters
		
		local tele_list = {}
		local tele_count = 0
		while tele_count < 3 do
			local index = math.random(#node_list)
			if not tele_list[index] then
				node_list[index].node_type = "teletransporter"
				tele_list[index] = true
				tele_count = tele_count + 1
			end
		end	
	end
	
	--// Connect all nodes that are a distance less than or equal to max radius
	function mapper:generate_links()
		assert(node_list, "Error: node list not created yet, call mapper:generate_nodes()")
		
		connections = {}
		
		for index1,node1_data in ipairs(node_list) do
			for index2=index1+1,#node_list do
				local node2_data = node_list[index2]
				local dx = math.abs(node2_data.x - node1_data.x)
				local dy = math.abs(node2_data.y - node1_data.y)
				
				if dx <= MAX_RADIUS then --do x first because screen width is wider than height, weeds out more right away
					if dy <= MAX_RADIUS then
						local dist = dx*dx + dy*dy --only take square root of ones within range to reduce unnecessary processing
						if dist <= MAX_RADIUS_SQ then --distance of the nodes is less than max radius
							--link nodes together
							dist = math.sqrt(dist)
							node1_data.connections[index1] = dist
							node2_data.connections[index2] = dist
							connections[#connections + 1] = {index1, index2}
						end
					end
				end
			end
		end
		
		return connections
	end
	
	--// Draws the paths connecting nodes
		--returns a sol.surface with all the connected paths drawn on it
	function mapper:generate_map()
		assert(node_list, "Error: node list not created yet, call mapper:generate_nodes()")
		assert(connections, "Error: connections not created yet, call mapper:generate_links()")
		
		local surface = sol.surface.create()
		
		--create blank table to keep track of map tiles
		local tiles_map = {}
		for y = 1, NUM_ROWS do
			local row = {}
			for x = 1, NUM_COLUMNS do row[x] = 0 end
			
			row.count = 0
			tiles_map[y] = row
		end
		
		--calculate pixel path of each node connection
		for _,link in ipairs(connections) do
			local node1 = node_list[ link[1] ]
			local node2 = node_list[ link[2] ]
			
			local path = util.make_path(node1, node2)
			
			--draw path on surface and save tiles
			for y,row in pairs(path) do --order indeterminate
				local x = row.start
				local width = row.stop - x + 1
				
				surface:fill_color({100,100,100}, x, y, width, 1) --surface showing all connections
				
				for i = 0, width-1 do tiles_map[y][x+i] = 1 end
			end
		end
		
		--generate map tiles for paths connecting nodes
		for y,row in ipairs(tiles_map) do
			local start_x, start_y, width
			
			for x,pixel in ipairs(row) do
				if pixel == 1 then
					if not width then
						start_x = 16*x - 16 --convert to map coordinates
						start_y = 16*y - 16 --convert to map coordinates
						width = 1
					else width = width + 1 end
				elseif pixel == 0 then
					if width then
						add_tile{
							x = start_x,
							y = start_y,
							width = width*16,
							height = 16, --always create strips 1 tile high
							pattern = TILES_LOOKUP.path,
						}
						
						start_x, start_y, width = nil, nil, nil
					end
					
					local adj_pixels = get_adjacent_pixels(tiles_map, x, y)
					local pattern = TILES_LOOKUP[adj_pixels]
					
					if pattern then
						add_tile{
							x = 16*x - 16, --convert to map coordinates
							y = 16*y - 16, --convert to map coordinates
							width = 16, height = 16,
							pattern = pattern,
						}
					end
				end
			end
			
			if width then
				add_tile{
					x = start_x,
					y = start_y,
					width = width*16,
					height = 16, --always create strips 1 tile high
					pattern = TILES_LOOKUP.path,
				}
			end
		end
		
		--generate map tiles for nodes
		for _,node in ipairs(node_list) do
			add_tile{
				x = 16*node.x - 16, --convert to map coordinates
				y = 16*node.y - 16, --convert to map coordinates
				width = 16,
				height = 16,
				pattern = TILES_LOOKUP.node,
			}
		end
		
		write_map()
		
		print("number of tiles:", #tiles_data, "number of nodes:", #node_list)
		
		return surface
	end
	
	return mapper
end

return map_generator

--[[ Copyright 2019 Llamazing
  [[ 
  [[ This program is free software: you can redistribute it and/or modify it under the
  [[ terms of the GNU General Public License as published by the Free Software Foundation,
  [[ either version 3 of the License, or (at your option) any later version.
  [[ 
  [[ It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [[ without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [[ PURPOSE.  See the GNU General Public License for more details.
  [[ 
  [[ You should have received a copy of the GNU General Public License along with this
  [[ program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
