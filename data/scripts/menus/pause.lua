--[[ pause.lua
	version 0.1a1
	12 Jan 2019
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	The menu script displays a pause menu.
	
										more to come...
]]

local language_manager = require"scripts/language_manager"

local pause_builder = {}

--// Creates a new menu instance
	--properties (table, key/value) - specifies configuration settings for the initialization of the menu (see below)
	--returns a newly created distance menu
function pause_builder:create(properties)
	local menu = {
		x = 0, y = 0, --(number, integer) coordinates in pixels used to apply a movement to the menu
		pos_x = math.floor(tonumber(properties.x) or 256), --x coordinate of where to draw the menu in pixels, negative values are from the right or the screen
		pos_y = math.floor(tonumber(properties.y) or 100), --y coordinate of where to draw the menu in pixels, negative values are from the bottom of the screen
		
		font = properties.font or "8_bit", --name of the font resource to use for displaying the text
		horizontal_alignment = properties.horizontal_alignment or "center", --string with possible values "left", "right" or "center"
		vertical_alignment = properties.vertical_alignment or "top", --string with possible values "top", "middle", "bottom"
		
		pause_text = nil, --sol.text_surface that displays "PAUSED"
		text1 = nil,
		text2 = nil,
	}
	
	
	--// Validation
	
	assert(type(menu.font)=="string", "Bad property font to 'pause_builder' (string expected)")
	assert(sol.main.resource_exists("font", menu.font), "Bad property font to 'pause_builder', invalid font name: "..menu.font)
	
	assert(type(menu.horizontal_alignment)=="string", "Bad property horizontal_alignment to 'pause_builder' (string expected)")
	assert(type(menu.vertical_alignment)=="string", "Bad property vertical_alignment to 'pause_builder' (string expected)")
	
	
	--// Implementation
	
	menu.pause_text = sol.text_surface.create{
		font = menu.font,
		horizontal_alignment = menu.horizontal_alignment,
		vertical_alignment = menu.vertical_alignment,
	}
	
	do
		local font, font_size = language_manager:get_menu_font(sol.language.get_language())
		local text_properties = {
			font = font,
			font_size = font_size,
			horizontal_alignment = "center",
			vertical_alignment = "top",
		}
		menu.text1 = sol.text_surface.create(text_properties)
		menu.text2 = sol.text_surface.create(text_properties)
	end
	
	--// Get/set the position of the menu
	function menu:get_xy() return self.x, self.y end
	function menu:set_xy(x, y)
		self.x = math.floor(tonumber(x) or 0)
		self.y = math.floor(tonumber(x) or 0)
	end
	
	function menu:on_started()
		self.pause_text:set_text(sol.language.get_string"pause.paused")
		self.text1:set_text(sol.language.get_string"pause.instructions")
		self.text2:set_text(sol.language.get_string"pause.view_map")
	end
	
	function menu:on_draw(dst_surface)
		dst_surface:fill_color{27, 27, 27, 200}
		
		local pos_x, pos_y = self.pos_x, self.pos_y
		local dst_width, dst_height = dst_surface:get_size()
		
		if pos_x < 0 then pos_x = pos_x + dst_width end
		if pos_y < 0 then pos_y = pos_y + dst_height end
		
		self.pause_text:draw(dst_surface, self.x + pos_x , self.y + pos_y)
		self.text1:draw(dst_surface, 256, 200)
		self.text2:draw(dst_surface, 256, 220)
	end

	return menu
end

setmetatable(pause_builder, {__call = pause_builder.create})

return pause_builder

--[[ Copyright 2019 Llamazing
  [[ 
  [[ This program is free software: you can redistribute it and/or modify it under the
  [[ terms of the GNU General Public License as published by the Free Software Foundation,
  [[ either version 3 of the License, or (at your option) any later version.
  [[ 
  [[ It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [[ without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [[ PURPOSE.  See the GNU General Public License for more details.
  [[ 
  [[ You should have received a copy of the GNU General Public License along with this
  [[ program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
