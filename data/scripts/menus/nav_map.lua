--[[ nav_map.lua
	version 0.1a1
	10 Feb 2019
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This menu script displays a view of the map where the player designate the destination
	coordinates for an NPC that will find a route using a network of navigation beacons as
	a guide.
]]

local map_generator = require"scripts/map_generator"
local node_map = require"scripts/node_map"
local util = require"scripts/util"

local nav_map_builder = {}

function nav_map_builder:create(game, properties)
	local menu = {}
	
	local is_visible = false
	local is_stay_open = false
	local tutorial_timer --replay intro dialog a second time if player is struggling
	
	--make procedurally generated map
	local mapper = map_generator:create()	
	local nodes = mapper:generate_nodes()
	mapper:designate_teletransporters(3)
	local connections = mapper:generate_links()
	local links_surface = mapper:generate_map()
	
	local player_marker = sol.surface.create(9,9)
	player_marker:fill_color({238, 170, 0}, 0, 0, 3, 2)
	player_marker:fill_color({255, 238, 0}, 1, 1, 1, 2)
	player_marker:set_xy(-1, -1) --move off-screen so doesn't leave a mark in upper left corner intially
	
	local bg_surface = sol.surface.create()
	local node_surface = sol.surface.create()
	local route_surface = sol.surface.create()
	
	--// draw the static background image one time (after intro), includes paths and nodes
	local function draw_bg_surface()
		--draw all nodes
		node_surface:clear()
		for i,node in ipairs(nodes) do
			local x = node.x
			local y = node.y
			
			if node.node_type=="teletransporter" then --custom property "node_type" has value "teletransporter" (node acts as a teletransporter)
				node_surface:fill_color({255,255,0}, x, y, 1, 1) --yellow for teletransporter nodes
			else node_surface:fill_color({255,0,255}, x, y, 1, 1) end --magenta for regular nodes
		end
		
		--draw static bg_surface with paths and nodes
		bg_surface:clear()
		bg_surface:fill_color{0,20,40,180}
		links_surface:draw(bg_surface)
		node_surface:draw(bg_surface)
	end
	
	--// moves the hero marker icon to the specified coordinates on the nav map, use to synchronize position with the hero
	function menu:set_player_position(x, y)
		--first mark previous location on map
		local prev_x, prev_y = player_marker:get_xy()
		route_surface:fill_color({200, 0, 0}, prev_x, prev_y, 1, 1)
		
		--move marker to new location
		x = (x-8)*(511/8176) + 1
		y = (y-13)*(287/4592) + 1
		player_marker:set_xy(x, y)
	end
	
	function menu:get_player_position() return menu:get_xy() end
	
	function menu:on_command_pressed(command)
		if command=="action" then
			if tutorial_timer~=false then --Robyn has moved
				game:start_dialog"intro_instructions_alt"
			else game:start_dialog"intro_success" end
			
			return true
		end
	end
	
	function menu:on_key_pressed(key)
		if key=="m" then
			is_visible = not is_visible
			is_stay_open = is_visible
			
			return true
		end
	end
	
	--// timer callback to continually check mouse coordinates while mouse button is held down
	local function mouse_update()
		if not sol.input.is_mouse_button_pressed"left" then return false end
		if not is_visible then return false end
		
		local hero = game:get_hero()
		local hero_x, hero_y, hero_layer = hero:get_position()
		
		local mouse_x, mouse_y = sol.input.get_mouse_position()
		mouse_x, mouse_y = mouse_x*16 - 8, mouse_y*16 - 8 --convert mouse quest coordinates to map coordinates
		if mouse_x >= 0 and mouse_y >= 0 then --TODO check if mouse is off screen
			local route = node_map:find_route(hero_x, hero_y, hero_layer, mouse_x, mouse_y, hero_layer) --TODO mouse click layer matches hero's layer, should actually determine layer where clicked instead?
			if route then
				route_surface:clear() --erase any old paths
				
				--note: route contains map coordinates which need to be converted to quest size coordinates
				for i,coords1 in ipairs(route) do
					--convert coordinates at i position
					coords1.x = (coords1.x-8)*(511/8176) + 1
					coords1.y = (coords1.y-13)*(287/4592) + 1
					
					if i > 1 then
						local coords2 = route[i-1] --coords already converted
						
						if not coords1.action then --don't draw path for teletransporters
							local path = util.make_path(coords1, coords2)
							
							--draw each path on surface
							for y,row in pairs(path) do --order indeterminate
								local x = row.start
								local width = row.stop - x + 1
								
								route_surface:fill_color({0,100,200}, x, y, width, 1) --surface showing all connections
							end
						end
					end
				end
				
				node_surface:draw(route_surface) --so route doesn't cover up nodes
			end
		end
		
		return true
	end
	
	local mouse_timer
	function menu:on_mouse_pressed(button, x, y) --TODO prevent mouse click during opening sequence
		if game:is_dialog_enabled() then return false end
		if button ~= "left" then return false end --only monitor left mouse button clicks
		
		local map = game:get_map()
		map:stop_movement() --make hero abort existing movement
		
		is_visible = true
		if mouse_timer then mouse_timer:stop() end
		
		local quest_width, quest_height = sol.video.get_quest_size()
		mouse_timer = sol.timer.start(self, 100, mouse_update)
	end
	
	function menu:on_mouse_released(button, x, y)
		if button ~= "left" then return false end --only monitor left mouse button clicks
		
		if mouse_timer then
			mouse_timer:stop()
			mouse_timer = nil
		end
		
		if is_visible then
			local map = game:get_map()
			
			map:set_destination(x*16 - 8, y*16 - 8, 0) --maps route to destination
			map:do_next_movement()
			
			--tutorial timer no longer needed once player figures out how to move Robyne
			if tutorial_timer then
				tutorial_timer:stop()
				tutorial_timer = false --false indicates Robyne has been given a route
			end
			
			if not is_stay_open then is_visible = false end
		end
		
		return game:is_dialog_enabled() --only block other menus from seeing the event while this menu is active
	end
	
	--draw function during the intro only
	function menu:on_draw(dst_surface)
		bg_surface:clear() --use as primary surface during intro
		bg_surface:fill_color{0,0,0}
		links_surface:draw(bg_surface) --not visible initially
		node_surface:draw(bg_surface) --add nodes gradually, initially blank
		player_marker:draw(bg_surface, -1, -2) --not visible initially
		bg_surface:draw(dst_surface)
	end
	
	--draw function to use after intro
	local function on_draw(self, dst_surface)
		if is_visible then
			bg_surface:draw(dst_surface)
			route_surface:draw(dst_surface)
			player_marker:draw(dst_surface, -1, -2)
		end
	end
	
	--// Opening cutscene that plays when launching the quest
	local function do_opening_sequence()
		local map = game:get_map()
		
		--the delay to use between placing node(s)
			--key: if the number of nodes placed is equal to the key, then start using this entry
			--value: the amount of the delay in milliseconds
		local NODE_DELAYS = {
			[0] = 500, --2 sec
			[4] = 250, --1.5 sec
			[10] = 200, --1 sec
			[15] = 150, --0.75 sec
			[20] = 100, --0.5 sec
			[25] = 50, --0.25 sec
			[30] = 20,
				--6000 sec for first 30 nodes
		}
		
		--how many nodes to place at a time
			--key: the maximum number of nodes present to use this entry
			--value: how many nodes to place each time
		local NODE_MULTIPLIERS = {
			[0] = 1, --6000 ms
			[30] = 2, --700 ms
			[100] = 3, --2000 ms
			[400] = 4, --2000 ms
			[800] = 5,
		}
		
		local MIN_SOUND_DELAY = 120 --in ms, don't play sound again for at least this long
		local time_since_last_sound = -1000 --negative to ensure first attempt to play sound won't be hindered
		
		--tidy up after intro sequence is done
		local function clean_up()
			menu.on_draw = on_draw --replace draw function from opening sequence
			bg_surface:set_transformation_origin(0, 0) --reset transformation origin
			bg_surface:set_scale(1, 1) --reset scale
			bg_surface:fade_in(0)
			draw_bg_surface()
		end
		
		--zooms in the nav map to where the camera is looking
		local function zoom_in()
			local camera = map:get_camera()
			x, y = camera:get_position()
			
			local offset_x = (x+8)%16
			local offset_y = (x+8)%16
			
			x = math.floor((x+8)*512/16/480)+1
			y = math.floor((y+8)*288/16/270)+1
			
			bg_surface:set_transformation_origin(x,y)
			local scale = 1
			sol.timer.start(map, 20, function()
				while scale < 16 do
					scale = math.min(scale + (scale + 2)/150, 16)
					bg_surface:set_scale(scale, scale)
					return true
				end
				
				
	--** End of Sequence **--
				
				--fade out the surface and close the menu when done
				bg_surface:fade_out(100, function()
					clean_up()
					game:start_dialog("intro_greeting", function()
						tutorial_timer = sol.timer.start(map, 30000, function()
							game:start_dialog"intro_instructions" --replay instructions a second time if player doesn't figure it out
						end)
					end)
				end)
			end)
		end
		
		local function play_sound(id)
			local timestamp = sol.main.get_elapsed_time()
			if time_since_last_sound + MIN_SOUND_DELAY <= timestamp then
				sol.audio.play_sound(id)
				time_since_last_sound = timestamp
				
				return true
			end
			
			return false
		end
		
		local node
		local node_iter,_,node_index = ipairs(nodes)
		--// adds the specified number of nodes to be displayed on screen, returns nil when no more can be added
		local function add_node(amount)
			amount = amount or 1
			
			local is_node_added = false --tentative
			for n = 1, amount do --repeat this number of times
				if not node_index then return end
				node_index, node = node_iter(nodes, node_index)
				
				if node then
					local x = node.x
					local y = node.y
					
					if node.node_type=="teletransporter" then
						node_surface:fill_color({255,255,0}, x, y, 1, 1) --yellow for teletransporters
					else node_surface:fill_color({255,0,255}, x, y, 1, 1) end --magenta for regular nodes
					is_node_added = true
				end
			end
			
			return is_node_added
		end
		
		--hide surfaces initially
		links_surface:set_opacity(0)
		player_marker:set_opacity(0)
		
		local delay, multiplier
		sol.timer.start(game:get_map(), 500, function()
			--update delay and multiplier values based on current node count
			multiplier = NODE_MULTIPLIERS[node_index] or multiplier
			delay = NODE_DELAYS[node_index] or delay
			
			--start to fade in links image once exactly 800 nodes are added
			if node_index == 800 then
				links_surface:set_opacity(255)
				links_surface:fade_in(120, function()
					player_marker:set_opacity(255)
					sol.timer.start(map, 750, function() zoom_in() end) --begin zoom in
				end)
			end
			
			if add_node(multiplier) then --add this many nodes
				play_sound"add_node" --only if node was added successfully
			end
			
			return node and delay or false --abort timer if no nodes remaining, else repeat with given delay
		end)
	end
	
	--launch menu and begin opening cutscene when map is first loaded
	local map_meta = sol.main.get_metatable"map"
	map_meta:register_event("on_started", function(self)
		do_opening_sequence()
		--cleanup:
		--[[
			menu.on_draw = on_draw --replace draw function from opening sequence
			bg_surface:set_transformation_origin(0, 0) --reset transformation origin
			bg_surface:set_scale(1, 1) --reset scale
			bg_surface:fade_in(0)
			draw_bg_surface()
			]]
	end)
	
	return menu
end

return nav_map_builder

--[[ Copyright 2019 Llamazing
  [[ 
  [[ This program is free software: you can redistribute it and/or modify it under the
  [[ terms of the GNU General Public License as published by the Free Software Foundation,
  [[ either version 3 of the License, or (at your option) any later version.
  [[ 
  [[ It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [[ without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [[ PURPOSE.  See the GNU General Public License for more details.
  [[ 
  [[ You should have received a copy of the GNU General Public License along with this
  [[ program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
