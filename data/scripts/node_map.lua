--[[ node_map.lua
	version 0.1a1
	10 Feb 2019
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script uses a network of navigation beacons (custom entity) on the current map to
	find a route for a given entity to the specified map coordinates (both must be located
	within the bounds of the navigation network).
	
	Designating nodes in the quest editor:
	* A node is a custom entity with the prefix "node_"
	* Assigning it the custom property "layers" and specifying a list of layer numbers for
	  the value (separated by commas, spaces or both) links the node to those layers, such
	  that an entity at that node can change between those layers freely like using stairs
	* Assigning the custom property "node_type" with value "teletransporter" links it with
	  all other nodes with the teletransporter value, allowing for fast travel among nodes
	  
]]

require"scripts/multi_events"

local node_map = {}

local map
local node_list
local nodes_xy --(table, key/value) list of nodes by position: nodes_xy[y][x] has the node as the key and a table listing the layers as the value
local teletransporter_list --(table, key/value) nodes as keys, value is true if the node type is a teletransporter

--variables updated on map change
local map_width, map_height --in pixels

--constants
local MAX_RANGE = 208 --visibility range for the NPC in pixels. Will navigate directly to nodes within this distance. (208=13*16)
local MAX_RANGE_SQ = MAX_RANGE*MAX_RANGE

--// converts map entity user-defined property string to a table array
--// each item in the property string should be separated by commas or spaces
	--str (string) - the value string of the property (to convert)
	--is_array (boolean, optional) - true: output table array, false: output key/value table with values of true
		--default: true (output table array)
local function property_to_table(str, is_array, convert_func)
	is_array = not is_array==false
	local list = {}
	if type(str)=="string" then
		str = str.."," --add comma to end so gmatch will get last entry
		for v in str:gmatch"([_%w]+)[ ,]+" do
			if convert_func then v = convert_func(v) end
			
			if is_array then
				list[#list + 1] = v
			else list[v] = true end
		end
	end
	
	return list
end

--// finds all nodes within the current navigation network, i.e. nodes within range of a node within range, etc.
function node_map:scan_all_nodes()
	assert(map, "Error in 'scan_all_nodes', no active map")
	
	node_list = {} --clear previous
	nodes_xy = {} --clear previous
	teletransporter_list = {} --clear previous
	
	local node_layers = {}
	
	--find all nodes in current map
	for node1 in map:get_entities"node_" do
		local index1 = #node_list + 1
		local connections = {}
		local x1, y1, layer1 = node1:get_position()
		local layers1 = property_to_table(node1:get_property"layers", false, tonumber)
		layers1[layer1] = true
		node_layers[node1] = layers1 --save for quick lookup on later cycles
		local node_type1 = node1:get_property"node_type"
		if node_type1 == "teletransporter" then --all teletransporter nodes are linked with every other teletransporter node
			teletransporter_list[node1] = true
		end
		
		--check distance to all previous nodes and make connections
		for index2,node2_data in ipairs(node_list) do
			local node2 = node2_data.node
			local x2, y2, layer2 = node2:get_position()
			
			
			local dx = math.abs(x2 - x1)
			if teletransporter_list[node1] and teletransporter_list[node2] then --both nodes are teletransporters, connect with 0 distance
				connections[index2] = 1
				node_list[index2].connections[index1] = 1
			elseif dx <= MAX_RANGE then --first compare just x & y deltas to eliminate most nodes with minimal processing
				local dy = math.abs(y2 - y1)
				if dy <= MAX_RANGE then
					local dist_sq = dx*dx + dy*dy
					if dist_sq <= MAX_RANGE_SQ then --nodes are within range
						if layer1==layer2 then
							--connect node 1 and node 2
							local distance = math.sqrt(dist_sq)
							connections[index2] = distance
							node_list[index2].connections[index1] = distance
						else --may still be connected if one of the nodes are "stairs"
							local layers2 = node_layers[node2]
							
							for lay1, _ in pairs(layers1) do
								if layers2[lay1] then --nodes have a common layer
									local distance = math.sqrt(dist_sq)
									connections[index2] = distance
									node_list[index2].connections[index1] = distance
									break
								end
							end
						end
					end
				end
			end
		end
		
		--create new node
		local data = {node=node1, index=index1, connections=connections}
		node_list[index1] = data
		node_list[node1] = data
		
		if not nodes_xy[y1] then nodes_xy[y1] = {} end
		if not nodes_xy[y1][x1] then nodes_xy[y1][x1] = {} end
		nodes_xy[y1][x1][node1] = layers1
	end
end

--// assigns cumulative distance of route for every node to specified target coordinates
function node_map:new_mapping(dst_x, dst_y, dst_layer)
	if not node_list then self:scan_all_nodes() end
	
	local mapping = {} --new instance to return
	local mapped_nodes --table or nil if not mapped
		--node entities (sol.entity) as keys and total route distance (number) from destination in pixels as value
		--sorted data contained as indicies sorted by increasing distance, where value is a table with the following keys/values:
			--node (sol.entity) - the node entity
			--distance (number) - the total distance of the route from this node entity to the destination
	local dest_x, dest_y, dest_layer
	
	--// generate mapping to the destination coordinates
		--dst_x (number, integer) - map x coordinate of the destination
		--dst_y (number, integer) - map y coordinate of the destination
	function mapping:set_destination(dst_x, dst_y, dst_layer)
		mapped_nodes = {} --clear old data
		local new_nodes = {}
		
		--validate destination coordinates
		dst_x = tonumber(dst_x)
		assert(dst_x, "Bad argument #2 to 'set_destination' (number expected)")
		dst_y = tonumber(dst_y)
		assert(dst_x, "Bad argument #3 to 'set_destination' (number expected)")
		dst_layer = tonumber(dst_layer)
		assert(dst_layer, "Bad argument #4 to 'set_destination' (number expected)")
		dest_x = math.floor(dst_x)
		dest_y = math.floor(dst_y)
		dest_layer = math.floor(dst_layer)
		
		--find nodes in range of destination
		local rect_x, rect_y = dst_x - MAX_RANGE, dst_y - MAX_RANGE
		local rect_size = MAX_RANGE*2 --width and height of rectangle
		for entity in map:get_entities_in_rectangle(rect_x, rect_y, rect_size, rect_size) do
			local name = entity:get_name()
			if name and name:match"^node_" then --is a node
				local x, y, layer = entity:get_position()
				
				local dx = math.abs(dst_x - x)
				if dx <= MAX_RANGE then
					local dy = math.abs(dst_y - y)
					if dy <= MAX_RANGE then
						local dist_sq = dx*dx + dy*dy
						if dist_sq <= MAX_RANGE_SQ then
							if layer==dst_layer then
								local distance = math.sqrt(dist_sq)
								if not mapped_nodes[entity] or distance < mapped_nodes[entity] then --only replace previous value if this value is lower
									mapped_nodes[entity] = distance
									new_nodes[entity] = true
								end
							else
								local layers = property_to_table(entity:get_property"layers", false, tonumber)
								if layers[dest_layer] then --links to destination layer
									local distance = math.sqrt(dist_sq)
									if not mapped_nodes[entity] or distance < mapped_nodes[entity] then --only replace previous value if this value is lower
										mapped_nodes[entity] = distance
										new_nodes[entity] = true
									end
								end
							end
						end
					end
				end
				
				local dy = math.abs(dst_y - y)
				local dist_sq = dx*dx + dy*dy
				local distance = math.sqrt(dist_sq)
			end
		end
		
		while next(new_nodes) do
			--new nodes from previous iteration become previous nodes this iteration
			local prev_nodes = new_nodes
			new_nodes = {}
			
			--follow connections of each previously added node to new nodes and add them to network
			for prev_node,_ in pairs(prev_nodes) do
				local prev_distance = mapped_nodes[prev_node]
				for connected_index,connected_distance in pairs(node_list[prev_node].connections) do
					local connected_node = node_list[connected_index].node
					local total_distance = prev_distance + connected_distance --total distance to connected node including previous route
					local route_distance = mapped_nodes[connected_node] --total distance to connected node using route from earlier iteration
					
					if not route_distance or total_distance < route_distance then
						mapped_nodes[connected_node] = total_distance --note: may be updating distance of a node added in previous iteration
						
						if not route_distance then new_nodes[connected_node] = true end --add to list of new nodes only if this is the first time at the node
					end
				end
			end
		end
		
		--note: any unreachable nodes won't be present in mapped_nodes
		
		--sort nodes by distance
		local nodes_sorted = {} --use intermediate table because don't want to add entries to mapped_nodes while traversing it
		for node,distance in pairs(mapped_nodes) do
			nodes_sorted[#nodes_sorted + 1] = {node=node, distance=distance}
		end
		table.sort(nodes_sorted, function(a,b) return a.distance < b.distance end)
		
		--merge sorted data back into mapped_nodes
		for i,data in ipairs(nodes_sorted) do mapped_nodes[i] = data end
		
		return mapping
	end
	
	--// Returns the x & y map coordinates of the mapped destination
	function mapping:get_destination() return dest_x, dest_y end
	
	--// Returns the number of nodes in the mapped node network (additional nodes on map that are out of range are not counted)
	function mapping:get_node_count() return #mapped_nodes end
	
	--// Returns the total route distance from the specified node entity to the destination coordinates
		--returns (number or nil) - the total route distance to destination in pixels or nil if the node is not part of the mapped node network
	function mapping:get_distance(node) return mapped_nodes[node] end
	
	--// Custom iterator that does not expose internal table used to get mapped nodes in order of ascending total route distance
		--usage: for i,node in mapping:iter_nodes() do
	function mapping:iter_nodes()
		local iter,_,start_val = ipairs(mapped_nodes)
		return function(_,i) return iter(mapped_nodes, i) end, {}, start_val
	end
	
	--// selects best node within range of given coordinates for plotting route to destination
		--x (number) - x coordinate of where to find best node from
		--y (number) - y coordinate of where to find best node from
		--layer (number) - map layer of where to find the best node from
		--returns:
			--(number) x coordinate of where to move to (either a node or the final destination)
			--(number) y coordinate of where to move to (either a node or the final destination)
			--(number) map layer of where to move to (either a node or the final destination)
			--(number) the distance in pixels that best node is away from the destination
			--(string or nil) a special action to be taken for the movement, possible values:
				--"teletransporter" - move to the next node instantly
	function mapping:next_node(x, y, layer)
		assert(mapped_nodes, "Error in 'next_node', a destination needs to be set using node_map:map()")
		
		--TODO validate inputs
		
		local best_node
		local best_layer
		local best_dist
		local special_action
		
		--find all nodes at current position
			--* determine which layers are accessible via "stairs" nodes
			--* determine whether at a teletransporter node
		local layers = {[layer] = true} --include current layer in list
		local is_teletransporter = false
		for node_here,linked_layers in pairs(nodes_xy[y] and nodes_xy[y][x] or {}) do --all nodes at current x & y coordinates
			--make list of accessible layers at current position
			if linked_layers[layer] then --node is linked to current layer
				for linked_layer,_ in pairs(linked_layers) do layers[linked_layer] = true end --any layer accessible via the linked layer is accessible
			end
			
			--find the node type if on the current layer
			local _,_,node_layer = node_here:get_position()
			if node_layer==layer then --node is at current layer
				local node_type = node_here:get_property"node_type"
				if node_type=="teletransporter" then is_teletransporter = true end
			end
		end
		
		--first check if current location is within range of destination
		local is_within_range = false --tentative
		if layers[dest_layer] then --doesn't matter if in range when on inaccessible layer
			local dx = x - dest_x
			local dx_sq = dx*dx
			if dx_sq <= MAX_RANGE_SQ then
				local dy = y - dest_y
				local dy_sq = dy*dy
				if dy_sq <= MAX_RANGE_SQ then
					local distance = math.sqrt(dx_sq + dy_sq)
					if distance <= MAX_RANGE then
						is_within_range = distance
					end
				end
			end
		end
		
		--find nodes in range of current position
		local rect_x, rect_y = x - MAX_RANGE, y - MAX_RANGE
		local rect_size = MAX_RANGE*2 --width and height of rectangle
		for entity in map:get_entities_in_rectangle(rect_x, rect_y, rect_size, rect_size) do
			local name = entity:get_name()
			if name and name:match"^node_" then --is a node
				local node_x, node_y, node_layer = entity:get_position()
				
				local dx = math.abs(x - node_x)
				if dx <= MAX_RANGE then
					local dy = math.abs(y - node_y)
					if dy <= MAX_RANGE then
						local dist_sq = dx*dx + dy*dy
						if dist_sq <= MAX_RANGE_SQ then
							local is_layer_match = layers[node_layer] and node_layer --value is the layer if it matches, otherwise false
							if not is_layer_match then --may still be connected if one of the nodes are "stairs"
								local node_layers = property_to_table(entity:get_property"layers", false, tonumber) --TODO look up saved data instead of parsing again
								for linked_layer,_ in pairs(layers) do --these layers are accessible from current position
									if node_layers[linked_layer] then --the node is also linked to the layer
										is_layer_match = linked_layer
										break
									end
								end
							end
							
							if is_layer_match then
								local node_distance = mapped_nodes[entity] --distance of node to destination
								if node_distance then --otherwise node is not reachable from current node network
									if best_node then
										if node_distance < best_dist then
											best_node = entity
											best_layer = is_layer_match
											best_dist = node_distance
										end
									else
										best_node = entity
										best_layer = is_layer_match
										best_dist = node_distance
									end
								end
							end
						end
						
					end
				end
			end
		end
		
		--teletransporters only: check connected teletransporters to see if better node is available
		if is_teletransporter then
			for tele_node,_ in pairs(teletransporter_list) do --check all teletransporter nodes
				local node_distance = mapped_nodes[tele_node] --distance of tele_node to destination
				if node_distance then
					local _,_,tele_layer = tele_node:get_position()
					if best_node then
						if node_distance < best_dist then
							best_node = tele_node
							best_layer = tele_layer
							best_dist = node_distance
							
							special_action = "teletransporter"
						end
					else
						best_node = tele_node
						best_layer = tele_layer
						best_dist = node_distance
						
						special_action = "teletransporter"
					end
				end
			end
		end
		
		if best_dist then
			local node_x, node_y = best_node:get_position()
			
			if is_within_range then
				--find coordinates nearest_x & nearest_y
				local nearest_x, nearest_y --coordinates along path to best_node that are closest to the destination
				if node_x == x then --divide by zero error on slope
					if node_y == y then return dest_x, dest_y, dest_layer, 0, nil end --best_node is the current node, head for destination directly
					nearest_x = x
					nearest_y = dest_y
				elseif node_y == y then --divide by zero error on slope reciprocal
					nearest_x = dest_x
					nearest_y = y
				else --can calculate slope normally
					local m = (node_y - y)/(node_x - x) --slope of line from current position to best_node
					local b1 = y - x*m --b value of line from current position to best_node
					local b2 = dest_y + dest_x/m --b value of line from nearest coordinates to final destination
					
					--nearest coordinates are intercept point of (Y = m*X + b1) and (Y = b2 - X/m)
					nearest_x = m*(b2 - b1)/(m*m + 1)
					nearest_y = b1 + m*nearest_x --solve for nearest_y given nearest_x
				end
				
				--find distance to coordinates nearest_x & nearest_y
				local dx = nearest_x - x
				local dy = nearest_y - y
				local nearest_dist_sq = dx*dx + dy*dy
				if nearest_dist_sq > 16 then --if farther than 4 pixels away from nearest coords then head to nearest coords
					--calculate distance from nearest coordinates to final destination
					dx = nearest_x - dest_x
					dy = nearest_y - dest_y
					local dest_dist = math.sqrt(dx*dx + dy*dy)
					
					return nearest_x, nearest_y, dest_layer, dest_dist, nil
				else return dest_x, dest_y, dest_layer, 0, nil end --head for final destination directly
			else return node_x, node_y, best_layer, best_dist, special_action end
		else return nil, nil, nil, nil, nil end --destination not reachable from current node network
	end
	
	--// Given start coordinates, invokes node_map:next_node() successively until end is reached
		--returns a table listing all the stops along the route (first entry is the beginning coords, final entry the end). Table keys:
			--x (number, non-negative) - x coordinate for this step of the route
			--y (number, non-negative) - y coordinate for this step of the route
			--layer (number, integer) - map layer for this step of the route
			--distance (number, non-negative) - distance along the route from this set of coordinates to the end coordinates
	function mapping:find_route(x_start, y_start, layer_start)
		local x_next, y_next, layer_next, next_distance, action = self:next_node(x_start, y_start, layer_start)
		if not x_next then return false end --cannot find route
		
		--calculate distance of first segment
		local dx = x_next - x_start
		local dy = y_next - y_start
		local prev_distance = math.sqrt(dx*dx + dy*dy) + next_distance
		
		local route = {{x=x_start, y=y_start, layer=layer_start, distance=prev_distance}} --add first coordinate pair
		
		while x_next do
			local x_prev, y_prev, layer_prev = x_next, y_next, layer_next
			
			--add next segment
			table.insert(route, {
				x=x_next, y=y_next, layer=layer_next,
				distance=next_distance, action=action,
			})
			
			if x_next==dest_x and y_next==dest_y and layer_next==dest_layer then --reached the last coordinates
				return route --success
			end
			
			x_next, y_next, layer_next, next_distance, action = self:next_node(x_next, y_next, layer_next)
			assert(x_next~=x_prev or y_next~=y_prev or layer_next~=layer_prev,
				"Error in 'find_route', endless loop detected"
			)
		end
		
		--couldn't find route from a middle segment (shouldn't happen)
		return false
	end
	
	return mapping:set_destination(dst_x, dst_y, dst_layer) --generates initial mapping to destination and returns new mapping instance created
end

--// Given start and stop coordinates, invokes node_map:next_node() successively until end is reached
	--returns a table listing all the stops along the route (first entry is the beginning coords, final entry the end). Table keys:
		--x (number, non-negative) - x coordinate for this step of the route
		--y (number, non-negative) - y coordinate for this step of the route
		--distance (number, non-negative) - distance along the route from this set of coordinates to the end coordinates
function node_map:find_route(x_start, y_start, layer_start, x_end, y_end, layer_end)
	local mapping = self:new_mapping(x_end, y_end, layer_end) --set destination coordinates
	return mapping:find_route(x_start, y_start, layer_start)
end

--// returns the number of nodes on the current map
function node_map:get_node_count()
	if not node_list then self:scan_all_nodes() end
	return #node_list
end

--when starting a map
map_meta = sol.main.get_metatable"map"
map_meta:register_event("on_started", function(self)
	map = self
	map_width, map_height = map:get_size()
	
	--delete old data
	node_list = nil
	
	node_map:scan_all_nodes() --find all custom entities used as nodes on map
end)

return node_map

--[[ Copyright 2019 Llamazing
  [[ 
  [[ This program is free software: you can redistribute it and/or modify it under the
  [[ terms of the GNU General Public License as published by the Free Software Foundation,
  [[ either version 3 of the License, or (at your option) any later version.
  [[ 
  [[ It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [[ without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [[ PURPOSE.  See the GNU General Public License for more details.
  [[ 
  [[ You should have received a copy of the GNU General Public License along with this
  [[ program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
