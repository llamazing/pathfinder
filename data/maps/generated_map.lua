-- Lua script of map overworld/open_field.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

require"scripts/multi_events"
local node_map = require"scripts/node_map"

local map = ...
local game = map:get_game()
local mapping

function map.victory()
	local sprite = robyne:get_sprite()
	sol.timer.start(map, 200, function() sol.audio.play_sound"jump" end)
	sprite:set_direction(3)
	sprite:set_animation("jumping", function()
		sprite.on_frame_changed = nil
		local shadow = robyne:get_sprite"shadow"
		if shadow then robyne:remove_sprite(shadow) end
		
		sprite:set_animation("victory", function()
			sprite:set_animation"stopped"
		end)
	end)
	
	function sprite:on_frame_changed(animation, frame)
		if animation=="jumping" and frame > 1 then
			self:set_xy(0, -3*(frame % 2) - 2) --height of 2 for frames 2 & 4, 5 for frame 3.
			
			--create shadow sprite
			if not robyne:get_sprite"shadow" then
				local shadow = robyne:create_sprite("entities/shadow", "shadow")
				shadow:set_animation"big"
				shadow:set_xy(0, -2)
				robyne:bring_sprite_to_back(shadow)
			end
		end
	end
end

local footsteps_timer --plays sound
--// Plays a footsteps sound while the walking animation is active
robyne:get_sprite().on_animation_changed = function(self, animation)
	if animation=="walking" then
		if not footsteps_timer then
			footsteps_timer = sol.timer.start(robyne, 280, function()
				sol.audio.play_sound"footsteps"
				return true
			end)
		end
	elseif footsteps_timer then
		footsteps_timer:stop()
		footsteps_timer = nil
	end
end

-- Event called at initialization time, as soon as this map is loaded.
map:register_event("on_started", function()
	hero:set_enabled(false)
	robyne:set_position(hero:get_position())
end)

function map:set_destination(dst_x, dst_y, dst_layer)
	mapping = node_map:new_mapping(dst_x, dst_y, dst_layer)
end

function map:do_next_movement()
	if not mapping then return end
	
	local x, y, layer, distance, action = mapping:next_node(robyne:get_position())
	if x then
		--shadow may still exist if starting a new movement from middle of jump, remove it
		local shadow = robyne:get_sprite"shadow"
		if shadow then robyne:remove_sprite(shadow) end
		
		robyne.target_x, robyne.target_y = x, y
		robyne.next_distance = distance
		
		if action == "teletransporter" then --do instant movement
			
			self:do_teletransport(x, y, layer)
			--self:do_next_movement()
		else --otherwise do normal movement
			local movement = sol.movement.create"target"
			movement:set_speed(96)
			movement:set_target(x, y)
			movement:start(robyne, function()
				if distance > 0 then
					self:do_next_movement()
				else
					sol.timer.start(map, 350, function() map.victory() end)
				end
			end)
		end
	else print"Cannot find route" end
end

function map:do_teletransport(x, y, layer)
	self:stop_movement()
	robyne:set_enabled(false)
	sol.audio.play_sound("warp")
	
	local camera = self:get_camera()
	local camera_x, camera_y = camera:get_position_to_track(x, y)
	
	local movement = sol.movement.create"target"
	movement:set_speed(600)
	movement:set_target(camera_x, camera_y)
	movement:start(camera, function()
		--bring Robyne to destination teletransporter and begin surprised animation
		robyne:set_position(x, y, layer)
		local sprite = robyne:get_sprite()
		sprite:set_direction(3)
		sprite:set_animation"surprised"
		robyne:set_enabled(true)
		sol.audio.play_sound("bomb")
		
		sol.timer.start(map, 500, function() --begin walking again after 0.5 second delay
			camera:start_tracking(robyne)
			map:do_next_movement()
		end)
	end)
end

function map:stop_movement()
	local movement = robyne:get_movement()
	if movement then movement:stop() end
	robyne:get_sprite():set_animation"stopped"
end

--always move the player to npc's location
robyne:register_event("on_position_changed", function(self, x, y, layer)
	--move hero and map marker to match
	hero:set_position(x, y, layer)
	game.nav_map:set_player_position(x, y)
	
	--update distance to target on HUD
	if self.target_x then
		local distance = self:get_distance(self.target_x, self.target_y) + self.next_distance
		game.distance_menu:set_distance(math.floor(distance/16))
	end
	
end)
