# Pathfinder

A navigation tool for Solarus that facilitates making a sequence of movements that follow a path of interconnected navigation beacons to specified destination coordinates using an optimal route.

### How to Use It

First use the Quest Editor to place custom entities representing the navigation beacons at key points along intended paths in your map. These beacons must have the prefix `node_` in their name. Place the beacons at road forks or at intermediate points along roads that are either long or not straight. The beacons are automatically connected to any other beacons that are within 13 tiles (there are some exceptions, see below). Think of the beacons as sign posts pointing in the direction(s) of nearby sign posts. Once you have a contiguous network of beacons placed, it is possible to find a route from any coordinates within range of any beacon to any other position in range of any beacon within the same network.

Then within the lua scripts of your quest you need to generate a "mapping" list targeting your final destination coordinates. This ranks each beacon in respect to its cumulative travel distance from the final destination. Then you simply call the `next_node()` method of the returned mapping object, passing it the current position of the entity you want to move. It returns the coordinates of the most optimal beacon within 13 tiles of your entity to move. So then you create a movement for your entity, and once it reaches that beacon, call `next_node()` again, this time passing its new position. The result is the coordinates to the next beacon that gets you even closer to the destination. Repeat this process until your entity eventually arrives at the final destination.

You need to create a separate mapping object for each final destination you want to navigate to. So if you have 3 entities all starting at the same coordinates but all with different final destinations, then you would need to generate 3 separate mapping objects. But if instead you have 3 entities starting at different coordinates, and they all have the same final destination coordinates, then you can use the same mapping object for all 3. You can continue to use the same mapping object as many times as you want, but it should be regenerated if the positions of any of the beacons move or if an obstacle gets added to the map which changes how the beacons are linked to one and another.

#### Special Beacons

Beacons are usually only connected if they are within 13 tiles of each other (208 pixels) and on the same layer.

**Stairs** beacons exist on multiple layers, providing a means to link beacons on separate layers.

**Teletransporter** beacons link to other nearby beacons like normal, but they also link to other teletransporter beacons regardless of the distance or layer. Travel from one teletransporter beacon to another teletransporter beacon is instantaneous.

### Example Quest

This repository includes an example quest that demonstrates the script in action. Simply launch Solarus and point it to the data directory of the repo.

A large map is procedurally generated with beacons distributed randomly. Choose the destination of an NPC by holding down the mouse button to reveal a mini-map and release with the cursor at the destination. The NPC's route is drawn on the mini-map, and the camera will follow the NPC while walking to the destination. More detailed instructions are given in the example quest.

![pathfinder_screenshot](https://gitlab.com/llamazing/pathfinder/-/wikis/uploads/efc6d509b076bbc2df35dcb4dcf2ca6e/pathfinder_screenshot.png)

### More Details

More detailed instructions for using the script can be found on the [How it works](https://gitlab.com/llamazing/pathfinder/-/wikis/how-it-works) wiki page.

The implementation for determining the most efficient route is described on the [Algorithm explained](https://gitlab.com/llamazing/pathfinder/-/wikis/algorithm-explained) wiki page.
